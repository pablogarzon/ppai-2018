package com.example.ppai_2018.modelos;

public class TipoCorteVacuno {
	private long id;
	private String nombre;
	private double precioXKilo;
	private String sigla;
	
	public TipoCorteVacuno() {
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getPrecioXKilo() {
		return precioXKilo;
	}
	public void setPrecioXKilo(double precioXKilo) {
		this.precioXKilo = precioXKilo;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
}
