package com.example.ppai_2018.modelos;

import java.util.Date;

import com.example.ppai_2018.patrones.state.EstadoCorte;

public class CorteVacuno {
	private int nroSecuencia;
	private double peso;
	private TipoCorteVacuno tipoCorte;
	private EstadoCorte estado;
	private Date fechaComercializacion;
	
	public CorteVacuno() {
	}
	
	public int getNroSecuencia() {
		return nroSecuencia;
	}
	public void setNroSecuencia(int nroSecuencia) {
		this.nroSecuencia = nroSecuencia;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public TipoCorteVacuno getTipoCorte() {
		return tipoCorte;
	}
	public void setTipoCorte(TipoCorteVacuno tipoCorte) {
		this.tipoCorte = tipoCorte;
	}
	public EstadoCorte getEstado() {
		return estado;
	}
	public void setEstado(EstadoCorte estado) {
		this.estado = estado;
	}
	public Date getFechaComercializacion() {
		return fechaComercializacion;
	}
	public void setFechaComercializacion(Date fechaComercializacion) {
		this.fechaComercializacion = fechaComercializacion;
	}
}
