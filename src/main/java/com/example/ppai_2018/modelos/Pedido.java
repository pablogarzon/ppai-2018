package com.example.ppai_2018.modelos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Pedido {
	private long numero;
	private Date fechaPedido;
	private List<DetallePedido> detalles;
	
	public Pedido() {
	}

	public long getNumero() {
		return numero;
	}

	public void setNumero(long numero) {
		this.numero = numero;
	}

	public Date getFechaPedido() {
		return fechaPedido;
	}

	public void setFechaPedido(Date fechaPedido) {
		this.fechaPedido = fechaPedido;
	}
	
	public List<DetallePedido> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<DetallePedido> detalles) {
		this.detalles = detalles;
	}
	
	/*
	 * obtiene los tipos de corte de los detalles del pedido
	 */
	public List<TipoCorteVacuno> obtenerTiposCorte() {
		List<TipoCorteVacuno> tipos = new ArrayList<>();
		for(DetallePedido detalle : this.detalles) {
			tipos.add(detalle.getTipoCorte());
		}
		return tipos;
	}
}
