package com.example.ppai_2018.modelos;

public class DetallePedido {
	private double kilosCorte;
	private TipoCorteVacuno tipoCorte;
	
	public DetallePedido() {
	}
	
	public double getKilosCorte() {
		return kilosCorte;
	}
	public void setKilosCorte(double kilosCorte) {
		this.kilosCorte = kilosCorte;
	}
	public TipoCorteVacuno getTipoCorte() {
		return tipoCorte;
	}
	public void setTipoCorte(TipoCorteVacuno tipoCorte) {
		this.tipoCorte = tipoCorte;
	}
}
