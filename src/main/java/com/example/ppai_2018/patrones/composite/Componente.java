package com.example.ppai_2018.patrones.composite;

import com.example.ppai_2018.patrones.iterator.*;

public interface Componente {
	
	public void agregar(Componente c);

	public String obtenerUbicacion();
	
	public Iterator crearIterator();
}
