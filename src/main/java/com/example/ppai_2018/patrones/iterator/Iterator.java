package com.example.ppai_2018.patrones.iterator;

public interface Iterator {

    public void primero();

    public boolean haTerminado();

    public Object actual();

    public void siguiente();

}
