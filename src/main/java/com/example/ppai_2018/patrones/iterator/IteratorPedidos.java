package com.example.ppai_2018.patrones.iterator;

import java.util.List;

import com.example.ppai_2018.modelos.Pedido;


public class IteratorPedidos implements Iterator {
	
	private List<Pedido> pedidos;
	private int posicion;
	
	public IteratorPedidos(List<Pedido> pedidos) {
		this.pedidos = pedidos;
	}
	
	public Object esPedidoSeleccionado(int nroPedido) {
		if(((Pedido) actual()).getNumero() == nroPedido) {
			return actual();
		} else {
			return null;
		}
	}

	@Override
	public void primero() {
		posicion = 0;
	}

	@Override
	public boolean haTerminado() {
		return !(posicion < pedidos.size() && pedidos.get(posicion) != null);
	}

	@Override
	public Object actual() {
		return pedidos.get(posicion);
	}

	@Override
	public void siguiente() {
		posicion++;
	}

}
