package com.example.ppai_2018.controladores;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.server.mvc.Viewable;

import com.example.ppai_2018.modelos.CorteVacuno;
import com.example.ppai_2018.modelos.Pedido;
import com.example.ppai_2018.modelos.TipoCorteVacuno;
import com.example.ppai_2018.patrones.iterator.IteratorPedidos;

@Path("generarRemito")
public class GestorGenerarRemito {

    private List<TipoCorteVacuno> tipos;
    private List<Pedido> pedidos;
    
    @GET
    public Viewable get() {
        return new Viewable("/index.ftl", "Fruit Index Page");
    }

    @GET
    @Path("/pedidos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Pedido> obtenerPedidos() {
        return pedidos;
    }

    @GET
    @Path("/tiposCorte/{nroPedido}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<TipoCorteVacuno> obtenerTiposCorte(@PathParam("nroPedido") int nroPedido) {
        List<TipoCorteVacuno> tipos = new ArrayList<>();
        IteratorPedidos it = new IteratorPedidos(pedidos);
        it.primero();
        while (!it.haTerminado()) {
            Pedido selecc = (Pedido) it.esPedidoSeleccionado(nroPedido);
            if (selecc != null) {
                tipos.addAll(selecc.obtenerTiposCorte());
            }
        }
        return tipos;
    }

    @GET
    @Path("/cortes")
    @Produces(MediaType.APPLICATION_JSON)
    public List<CorteVacuno> obtenerCortes() {
        return null;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void generarInforme(List<TipoCorteVacuno> tiposSelecc) {

    }
}
